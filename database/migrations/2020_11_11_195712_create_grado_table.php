<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Grado;

class CreateGradoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grado', function (Blueprint $table) {
            $table->id();
            $table->string('valor');
            $table->timestamps();
        });
        $record = new Grado;
        $record->valor = 1;
        $record->save();
        $record = new Grado;
        $record->valor = 2;
        $record->save();
        $record = new Grado;
        $record->valor = 3;
        $record->save();
        $record = new Grado;
        $record->valor = 4;
        $record->save();
        $record = new Grado;
        $record->valor = 5;
        $record->save();
        $record = new Grado;
        $record->valor = 6;
        $record->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grado');
    }
}

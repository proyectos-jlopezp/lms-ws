<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateConfiguracionTableImgLogin extends Migration
{
    public function up()
    {
        Schema::table('configuracion', function (Blueprint $table) {
            $table->string('imagen_login')->nullable();
        });
        
    }

    public function down()
    {
       
    }
}

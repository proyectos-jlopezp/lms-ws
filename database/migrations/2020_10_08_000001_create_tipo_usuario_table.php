<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateTipoUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_usuario', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->timestamps();
        });

        //ALUMNO
        DB::table('tipo_usuario')->insert([
            'id' => 1,
            'nombre' => 'ALUMNO'
        ]);
        //PROFESOR
        DB::table('tipo_usuario')->insert([
            'id' => 2,
            'nombre' => 'PROFESOR'
        ]);
        //ADMINISTRADOR
        DB::table('tipo_usuario')->insert([
            'id' => 3,
            'nombre' => 'ADMINISTRADOR'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_usuario');
    }
}

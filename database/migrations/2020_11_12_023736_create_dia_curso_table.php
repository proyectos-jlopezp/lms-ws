<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiaCursoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dia_curso', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('dia_id');
            $table->foreign('dia_id')->references('id')->on('dia');
            $table->unsignedBigInteger('aula_curso_id');
            $table->foreign('aula_curso_id')->references('id')->on('aula_curso')->onDelete('cascade');
            $table->time('hora_inicio');
            $table->time('hora_fin');
            $table->unsignedBigInteger('profesor_id')->nullable();
            $table->unsignedBigInteger('periodo_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dia_curso');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateEstadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estado', function (Blueprint $table) {
            $table->id();
            $table->string('valor');
            $table->timestamps();
        });

        //MATRICULADO
        DB::table('estado')->insert([
            'id' => 1,
            'valor' => 'MATRICULADO'
        ]);
        //NO MATRICULADO
        DB::table('estado')->insert([
            'id' => 2,
            'valor' => 'NO MATRICULADO'
        ]);
        //RETIRADO
        DB::table('estado')->insert([
            'id' => 3,
            'valor' => 'RETIRADO'
        ]);
        //EXPULSADO
        DB::table('estado')->insert([
            'id' => 4,
            'valor' => 'EXPULSADO'
        ]);
        //ACTIVO
        DB::table('estado')->insert([
            'id' => 5,
            'valor' => 'ACTIVO'
        ]);

        //NO ACTIVO
        DB::table('estado')->insert([
            'id' => 6,
            'valor' => 'NO ACTIVO'
        ]);
        //NUEVO
        DB::table('estado')->insert([
            'id' => 7,
            'valor' => 'NUEVO'
        ]);
        //NUEVO
        DB::table('estado')->insert([
            'id' => 8,
            'valor' => 'SUSPENDIDO'
        ]);
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estado');
    }
}

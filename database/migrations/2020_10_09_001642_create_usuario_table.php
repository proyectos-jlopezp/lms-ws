<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Usuario;
use App\Models\TipoUsuario;
use App\Models\Estado;


class CreateUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
            $table->id();
            $table->string('dni');
            $table->string('apellido_paterno');
            $table->string('apellido_materno');
            $table->string('nombre');
            $table->string('direccion');
            $table->string('telefono');
            $table->string('celular')->nullable();
            $table->string('correo');
            $table->string('imagen')->nullable();
            $table->boolean('firstLogin')->default(false); //False: Usuario nuevo que debe configurar contraseña
            $table->timestamp('fecha_nacimiento');
            $table->string('password');
            $table->unsignedBigInteger('tipo_usuario_id');
            $table->foreign('tipo_usuario_id')->references('id')->on('tipo_usuario');
            $table->unsignedBigInteger('estado_id')->default(5);
            $table->foreign('estado_id')->references('id')->on('estado');
            $table->unsignedBigInteger('apoderado_id')->nullable();
            $table->foreign('apoderado_id')->references('id')->on('apoderado');
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();
        });


        //Hardcoded admin user
        $profesor = new Usuario;
        $profesor->dni = "12345678";
        $profesor->apellido_paterno = 'admin';
        $profesor->apellido_materno = 'admin';
        $profesor->nombre = 'admin';
        $profesor->direccion =  'Av. Admin';
        $profesor->telefono = '123456789';
        $profesor->correo = 'admin@gmail.com';
        $profesor->password = bcrypt('d1r8idt43i6a2s5oa7nrm');
        $profesor->fecha_nacimiento = '2020-10-14 00:00:00';
        $profesor->estado_id = Estado::whereValor('ACTIVO')->first()->id;
        $profesor->tipo_usuario_id = TipoUsuario::whereNombre('ADMINISTRADOR')->first()->id;
        $profesor->firstLogin = true;
        $profesor->save();
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario');
    }
}

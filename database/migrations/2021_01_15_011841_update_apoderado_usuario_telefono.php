<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Apoderado;

class UpdateApoderadoUsuarioTelefono extends Migration
{

    public function up()
    {
        Schema::table('usuario', function (Blueprint $table) {
            $table->string('telefono')->nullable()->change();
        });
        Schema::table('apoderado', function (Blueprint $table) {
            $table->string('telefono')->nullable();
        });

        $apoderados = Apoderado::with(['alumnos'])->get();
        foreach ($apoderados as $apoderado) {
            $apoderado->telefono = $apoderado->alumnos[0]->telefono;
            $apoderado->save();
        }
    }

    public function down()
    {
        Schema::table('usuario', function (Blueprint $table) {
            $table->string('telefono')->nullable(false)->change();
        });
        Schema::table('apoderado', function (Blueprint $table) {
            $table->dropColumn('telefono');
        });
    }
}

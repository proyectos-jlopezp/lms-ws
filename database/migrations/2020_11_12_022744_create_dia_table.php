<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Dia;

class CreateDiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dia', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('nombre');
        });

        $record = new Dia;
        $record->nombre = "Lunes";
        $record->save();
        $record = new Dia;
        $record->nombre = "Martes";
        $record->save();
        $record = new Dia;
        $record->nombre = "Miercoles";
        $record->save();
        $record = new Dia;
        $record->nombre = "Jueves";
        $record->save();
        $record = new Dia;
        $record->nombre = "Viernes";
        $record->save();
        $record = new Dia;
        $record->nombre = "Sabado";
        $record->save();
        $record = new Dia;
        $record->nombre = "Domingo";
        $record->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dia');
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PassportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('oauth_clients')->insert([
            'name' => 'Laravel Personal Access Client',
            'secret' => 'D105aRCWkvbozp92CS4vgMNuleqQ26tH1jmFzgmH',
            'redirect' => 'http://localhost',
            'personal_access_client' => '1',
            'password_client' => '0',
            'revoked' => '0',
        ]);

        DB::table('oauth_clients')->insert([
            'name' => 'Laravel Password Grant Client',
            'secret' => 'B22msRGQlb25t7W0nwc8XMEfDdf2tVLGflXnLhjV',
            'provider' => 'users',
            'redirect' => 'http://localhost',
            'personal_access_client' => '0',
            'password_client' => '1',
            'revoked' => '0',
        ]);

        DB::table('oauth_personal_access_clients')->insert([
            'client_id' => 1,
        ]);

        DB::table('configuracion')->insert([
            'cPrincipal' => '#b00020',
            'cSecundario' => '#ff9052',
        ]);
    }
}

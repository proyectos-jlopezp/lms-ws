<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfiguracionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('configuracion')->insert([
            'cPrincipal' => '#b00020',
            'cSecundario' => '#ff9052',
        ]);
    }
}

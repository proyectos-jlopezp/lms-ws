<?php


namespace App\Utilities;

use Illuminate\Support\Facades\Log;

class ProxyRequest
{


    public function grantPasswordToken(string $dni, string $password)
    {
        /*Setea los parámetros necesarios para password grant y hacer la petición POST */
        $params = [
            'grant_type' => 'password',
            'username' => $dni,
            'password' => $password,
        ];

        return $this->makePostRequest($params);
    }

    public function refreshAccessToken()
    {
        /*Se verifica si es el Request contiene 'refresh_token' si es así se actualiza
        sino se aborta Request */
        $refreshToken = request()->cookie('refresh_token');
        abort_unless($refreshToken, 401, 'Tu token ha expirado');
        $params = [
            'grant_type' => 'refresh_token',
            'refresh_token' => $refreshToken,
        ];

        return $this->makePostRequest($params);
    }

    protected function makePostRequest(array $params)
    {
        /*Se agrega las variables del .env a los parámetros enviados */
        $params = array_merge([
            'client_id' => config('services.passport.password_client_id'),
            'client_secret' => config('services.passport.password_client_secret'),
            'scope' => '*',
        ], $params);
        /*se hace un método POST interno hacia las rutas de passport con los parámetros necesarios */
        $proxy = \Request::create('oauth/token', 'post', $params);
        
        /*Se recibe la respuesta de la ruta de passport */
        $resp = json_decode(app()->handle($proxy)->getContent());
        /*Se setea en la cookie el refresh_token */
        $this->setHttpOnlyCookie($resp->refresh_token);

        return $resp;
    }

    protected function setHttpOnlyCookie(string $refreshToken)
    {
        cookie()->queue(
            'refresh_token',
            $refreshToken,
            14400, // 10 days
            null,
            null,
            false,
            true // httponly
        );
    }
}

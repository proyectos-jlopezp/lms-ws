<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use App\Mail\UsuarioCreado;
use App\Models\Usuario;
use Illuminate\Support\Facades\Log;


class ProcessMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $usuario;
    protected $password;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Usuario $usuario, $password)
    {
        $this->usuario = $usuario;
        $this->password = $password;
    }

    public function enviarPassCorreo($password, Usuario $usuario)
    {
        try {
            $usuario->password = $password;
            Mail::to($usuario->correo)->send(new UsuarioCreado($usuario));
        } catch (\Exception $e) {
            Log::info('Excepcion en enviarPassCorreo:' . $e->getTraceAsString());
            throw $e;
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /*Envio de credenciales del usuario profesor */
        Log::info('pass desencriptada:' . $this->password);
        try {
            $this->enviarPassCorreo($this->password, $this->usuario);
            return response()->json(
                [
                    'status' => true,
                    'message' => 'Profesor guardado'
                ],
                200
            );
        } catch (\Exception $e) {
            $usuario = Usuario::find($this->usuario->id); // En caso falle, se elimina el usuario creado anteriormente
            $usuario->delete();
            /*return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error al enviar correo de credenciales al profesor.',
                    'body' => $e->getMessage()
                ],
                500
            );*/
        }
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class AulaCursoProfesor extends Pivot
{
    use HasFactory;
    protected $table = 'aula_curso_profesor';
    protected $fillable = [
        'aula_curso_id', 'profesor_id'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ 
           'created_at', 
        'updated_at'
    ];


}

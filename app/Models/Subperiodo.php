<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subperiodo extends Model
{
    protected $table = 'subperiodo';

    use HasFactory;

    protected $fillable = [
        'secuencia', 'anio_id',
    ];

    public function periodo()
    {
        return $this->belongsTo('App\Models\Periodo');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Periodo extends Model
{
    protected $table = 'periodo';

    use HasFactory;

    protected $fillable = [
        'nombre', 'modalidad', 'estado', 'inicio',
    ];

    public function subperiodos()
    {
        return $this->hasMany('App\Models\Subperiodo')->orderBy('id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Usuario extends Authenticatable
{

    public function findForPassport($username)
    {
        return $this->where('dni', $username)->first();
    }


    protected $table = 'usuario';

    use SoftDeletes;
    use HasFactory;
    use Notifiable, HasApiTokens;

    protected $fillable = [
        'dni','estado_id'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /* Relationship */

    public function tipo_usuario()
    {
        return $this->belongsTo('App\Models\TipoUsuario');
    }

    public function apoderado()
    {
        return $this->belongsTo('App\Models\Apoderado');
    }

    public function estado()
    {
        return $this->belongsTo('App\Models\Estado');
    }

    public function aulas_profesor()
    {
        return $this->belongsToMany('App\Models\AulaCurso', 'aula_curso_profesor', 'profesor_id')->using('App\Models\AulaCursoProfesor')->withPivot('id');
    }

    public function aulas_alumno()
    {
        return $this->belongsToMany('App\Models\Aula', 'alumno_aula', 'alumno_id', 'aula_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Capacidad extends Model
{
    protected $table = 'capacidad';

    use HasFactory;

    protected $fillable = [
        'nombre', 'estado',
    ];

    public function competencia()
    {
        return $this->belongsTo('App\Models\Competencia');
    }
}

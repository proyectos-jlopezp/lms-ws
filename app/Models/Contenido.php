<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contenido extends Model
{

    protected $table = 'contenido';


    use HasFactory;

    protected $fillable = [
        'titulo', 'data',
    ];

    public function aulacurso()
    {
        return $this->belongsTo('App\Models\AulaCurso');
    }

    public function subperiodo()
    {
        return $this->belongsTo('App\Models\Subperiodo');
    }
}

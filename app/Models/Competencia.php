<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Competencia extends Model
{

    protected $table = 'competencia';

    use HasFactory;

    protected $fillable = [
        'nombre', 'estado',
    ];

    public function curso()
    {
        return $this->belongsTo('App\Models\Curso');
    }
}

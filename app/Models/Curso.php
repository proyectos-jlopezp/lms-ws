<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{

    protected $table = 'curso';

    use HasFactory;

    protected $fillable = [
        'nombre', 'estado',
    ];

    public function area()
    {
        return $this->belongsTo('App\Models\Area');
    }

    public function aulas()
    {
        return $this->belongsToMany('App\Models\Aula', 'aula_curso', 'curso_id', 'aula_id');
    }
}

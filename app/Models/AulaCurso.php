<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AulaCurso extends Model
{
    use HasFactory;
    protected $table = 'aula_curso';

    protected $fillable = [
        'curso_id', 'aula_id',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'temario' //Campo 'temario' al ser binary no puede ser procesado como json
    ];


    public function curso(){
        return $this->belongsTo('App\Models\Curso');
    }    

    public function profesores()
    {
        return $this->belongsToMany('App\Models\Usuario', 'aula_curso_profesor', 'aula_curso_id', 'profesor_id')->withPivot('id');
    }

    public function horarios()
    {
        return $this->belongsToMany('App\Models\Dia', 'dia_curso', 'aula_curso_id', 'dia_id')->withPivot('hora_inicio', 'hora_fin','id');;
    }

    public function diacurso()
    {
        return $this->hasMany(DiaCurso::class);
    }

    public function aula()
    {
        return $this->belongsTo('App\Models\Aula', 'aula_id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{

    protected $table = 'area';

    use HasFactory;

    protected $fillable = [
        'nombre', 'estado',
    ];

    public function cursos()
    {
        return $this->hasMany('App\Models\Curso');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aula extends Model
{
    use HasFactory;
    protected $table = 'aula';

    protected $fillable = [
        'periodo_id','grado_id','seccion','nivel_id'
    ];

    public function cursos()
    {
        return $this->belongsToMany('App\Models\Curso','aula_curso','aula_id','curso_id')->withPivot('id');
    }

    public function grado(){
        return $this->belongsTo('App\Models\Grado');
    }

    public function nivel(){
        return $this->belongsTo('App\Models\Nivel');
    }

    public function periodo(){
        return $this->belongsTo('App\Models\Periodo');
    }    

    public function alumnos()
    {
        return $this->belongsToMany('App\Models\Usuario', 'alumno_aula', 'aula_id', 'alumno_id')->withPivot('id');
    }

    public function cursos_aula()
    {
        return $this->hasMany('App\Models\AulaCurso');
    }
}

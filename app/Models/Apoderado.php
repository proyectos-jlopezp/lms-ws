<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Apoderado extends Model
{
    protected $table = 'apoderado';

    use HasFactory;

    protected $fillable = [
        'dni', 'apellido_paterno', 'apellido_materno', 'nombre', 'celular', 'correo'
    ];

    public function alumnos()
    {
        return $this->hasMany('App\Models\Usuario');
    }
}

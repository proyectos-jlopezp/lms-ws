<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DiaCurso extends Model
{
    use HasFactory;
    protected $table = 'dia_curso';

    protected $fillable = [
        'dia_id', 'aula_curso_id', 'hora_inicio', 'hora_fin', 'profesor_id', 'periodo_id'
    ];

    /**
     * Get the post that owns the comment.
     */
    public function aula_curso()
    {
        return $this->belongsTo('App\Models\AulaCurso', 'aula_curso_id');
    }
}

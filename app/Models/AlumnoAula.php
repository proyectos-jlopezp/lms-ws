<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlumnoAula extends Model
{
    protected $table = 'alumno_aula';

    use HasFactory;

    protected $fillable = [
        'aula_id', 'alumno_id'
    ];

    public function aula()
    {
        return $this->belongsTo('App\Models\Aula');
    }

    public function alumno()
    {
        return $this->belongsTo('App\Models\Usuario','alumno_id','id');
    }

    public function alumnos()
    {
        return $this->hasMany('App\Models\Usuario','id','alumno_id');
    }
}

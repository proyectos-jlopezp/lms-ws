<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\ProcessMail;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\UsuarioRequest;
use App\Models\TipoUsuario;
use App\Models\Usuario;


class AdminController extends UsuarioController
{
    //

    public function all()
    {
        try {
            /*Retornar solo los usuarios tipo profesor y con el valor de su estado */
            $usuarios = Usuario::with(['estado'])->whereHas('tipo_usuario', function ($q) {
                $q->where('id', '=', TipoUsuario::whereNombre('ADMINISTRADOR')->first()->id);
            })->get()->each(function ($user) { /*Ignorar los campos innecesarios */
                $user->makeHidden(['estado_id', 'apoderado_id', 'tipo_usuario_id', 'imagen', 'created_at', 'updated_at', 'fecha_nacimiento']);
            });;

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Administradores Encontrados',
                    'body' => $usuarios
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    private function fillValuesAdmin(Request $request, Usuario $usuario)
    {
        //Datos del usuario
        $data = json_decode($request->data);
        $usuario->dni = $data->dni;
        $usuario->apellido_paterno = $data->apellido_paterno;
        $usuario->apellido_materno = $data->apellido_materno;
        $usuario->nombre = $data->nombre;
        $usuario->direccion = $data->direccion;
        $usuario->telefono = $data->telefono;
        $usuario->celular = $data->celular;
        $usuario->correo = $data->correo;
        $usuario->fecha_nacimiento = $data->fecha_nacimiento;
        $s = $usuario->fecha_nacimiento;
        $date = str_replace('/', '-', $s);
        $date = strtotime($date);
        $usuario->fecha_nacimiento =  date('Y-m-d', $date);
        $usuario->estado_id = $data->estado_id;

        //Imagen del usuario
        if ($request->hasFile('img')) {
            $usuario->imagen = $this->setProfileImage($request, 'img', 'profile' . $usuario->dni);
        }
    }


    public function add(UsuarioRequest $request)
    {

        /*Creacion del usuario administrador */
        try {
            $usuario = new Usuario;
            $this->fillValuesAdmin($request, $usuario);
            $user_password = $this->generatePassword();
            $usuario->password = bcrypt($user_password);
            /*  */

            $usuario->tipo_usuario_id = TipoUsuario::whereNombre('ADMINISTRADOR')->first()->id;
            $usuario->save();
        } catch (\Exception $e) {
            Log::info('estoy here excp');
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error al crear el usuario profesor.',
                    'body' => $e->getMessage()
                ],
                500
            );
        }

        //Envio de correo a través de JOBS        
        ProcessMail::dispatch($usuario, $user_password);
    }


    public function edit(Request $request)
    {
        try {
            $data = json_decode($request->data);
            $usuario = Usuario::find($data->id);
            $this->fillValuesAdmin($request, $usuario);
            $usuario->save();

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Administrador guardado'
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }


    public function delete($id)
    {
        try {
            $admin = Usuario::whereId($id)->first();
            if ($admin == null) {
                return response()->json(
                    [
                        'status' => false,
                        'message' => 'Usuario no existe.',

                    ],
                    200
                );
            } else {
                $admin->delete();
                return response()->json(
                    [
                        'status' => true,
                        'message' => 'Se elimino el usuario administrador.',
                    ],
                    200
                );
            }
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }
}

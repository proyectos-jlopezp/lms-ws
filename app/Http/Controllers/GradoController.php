<?php

namespace App\Http\Controllers;
use App\Models\Grado;
use Illuminate\Http\Request;

class GradoController extends Controller
{
    //
    public function all()
    {
        try
        {
            $records = Grado::all();

            return response()->json(['status' => true,
                'message' => 'Resultados Encontrados',
                'body' => $records],
                200);
                
        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }
}

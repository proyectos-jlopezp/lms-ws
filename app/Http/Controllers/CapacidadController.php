<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Capacidad;
use Illuminate\Support\Facades\Log;

class CapacidadController extends Controller
{
    //
    public function all()
    {
        try
        {
            $capacidades = Capacidad::with(['competencia'])->get();

            return response()->json(['status' => true,
                'message' => 'Cursos Encontrados',
                'body' => $capacidades],
                200);
        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }

    public function add(Request $request)
    {
        try
        {
            Log::info($request->area_id);

            //$area = Area::whereId($request->id_area)->first();
            //Log::info($area);

            $curso = new Curso;
            $curso->nombre = $request->nombre;
            $curso->estado = true;
            $curso->area_id = $request->area_id;
            $curso->save();

            
            return response()->json(['status' => true,
                'message' => 'Curso registrado',
                'body' => $curso],
                200);
        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }
}

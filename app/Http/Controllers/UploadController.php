<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use \Datetime;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    //
    public function image()
    {

        try {


            $fieldname = "file";
            // Get filename.
            $filename = explode(".", $_FILES[$fieldname]["name"]);

            // Validate uploaded files.
            // Do not use $_FILES["file"]["type"] as it can be easily forged.
            $finfo = finfo_open(FILEINFO_MIME_TYPE);

            // Get temp file name.
            $tmpName = $_FILES[$fieldname]["tmp_name"];


            // Get mime type.
            $mimeType = finfo_file($finfo, $tmpName);

            // Get extension. You must include fileinfo PHP extension.
            $extension = end($filename);

            // Allowed extensions.
            $allowedExts = array("gif", "jpeg", "jpg", "png", "svg", "blob");

            // Allowed mime types.
            $allowedMimeTypes = array("image/gif", "image/jpeg", "image/pjpeg", "image/x-png", "image/png", "image/svg+xml");

            // Validate image.
            if (!in_array(strtolower($mimeType), $allowedMimeTypes) || !in_array(strtolower($extension), $allowedExts)) {
                throw new \Exception("File does not meet the validation.");
            }
            // Generate new random name.
            $name = sha1(microtime()) . "." . $extension;
            $fullNamePath = realpath("") . '/uploads/' . $name;
            //$fullNamePath = "/home/johnny/Documentos/proyectos/LMS/lms-ws/storage/app/images/uploads/" . $name;

            // Check server protocol and load resources accordingly.
            if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
                $protocol = "https://";
            } else {
                $protocol = "http://";
            }


            // Save file in the uploads folder.
            move_uploaded_file($tmpName, $fullNamePath);

            // Generate response.
            $response = new \StdClass;
            $response->location = $protocol . $_SERVER["HTTP_HOST"] . "/uploads/" . $name;

            // Send response.
            return stripslashes(json_encode($response));
        } catch (Exception $e) {
            // Send error response.
            return $e->getMessage();
            http_response_code(404);
        }
    }

    public function video()
    {
        try {
           

            $fieldname = "file";

            // Get filename.
            $filename = explode(".", $_FILES[$fieldname]["name"]);

            // Validate uploaded files.
            // Do not use $_FILES["file"]["type"] as it can be easily forged.
            $finfo = finfo_open(FILEINFO_MIME_TYPE);

            // Get temp file name.
            $tmpName = $_FILES[$fieldname]["tmp_name"];

            // Get mime type. You must include fileinfo PHP extension.
            $mimeType = finfo_file($finfo, $tmpName);

            // Get extension.
            $extension = end($filename);

            // Allowed extensions.
            $allowedExts = array("mp4", "webm", "ogg");

            // Allowed mime types.
            $allowedMimeTypes = array("video/mp4", "video/webm", "video/ogg");

            // Validate file.
            if (!in_array(strtolower($mimeType), $allowedMimeTypes) || !in_array(strtolower($extension), $allowedExts)) {
                throw new \Exception("File does not meet the validation.");
            }

            // Generate new random name.
            $name = sha1(microtime()) . "." . $extension;
            $fullNamePath = realpath("") . "/uploads/" . $name;

            // Check server protocol and load resources accordingly.
            if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
                $protocol = "https://";
            } else {
                $protocol = "http://";
            }

            // Save file in the uploads folder.
            move_uploaded_file($tmpName, $fullNamePath);

            // Generate response.
            $response = new \StdClass;
            $response->location = $protocol . $_SERVER["HTTP_HOST"] . "/uploads/" . $name;

            // Send response.
            return stripslashes(json_encode($response));
        } catch (Exception $e) {
            // Send error response.
            return $e->getMessage();
            http_response_code(404);
        }
    }

    public function file()
    {
        try {
            $fieldname = "file";

            // Get filename.
            $filename = explode(".", $_FILES[$fieldname]["name"]);
            // Validate uploaded files.
            // Do not use $_FILES["file"]["type"] as it can be easily forged.
            $finfo = finfo_open(FILEINFO_MIME_TYPE);

            // Get temp file name.
            $tmpName = $_FILES[$fieldname]["tmp_name"];

            // Get mime type. You must include fileinfo PHP extension.
            $mimeType = finfo_file($finfo, $tmpName);

            //Get name
            $nombre = reset($filename);
            // Get extension.
            $extension = end($filename);

            // Allowed extensions.
            $allowedExts = array("txt", "pdf", "doc", "json", "html", "docx", "xls", "xlsx", "ppt", "pptx", "mp3");

            // Allowed mime types.
            $allowedMimeTypes = array("text/plain", "application/msword", "application/x-pdf", "application/pdf", "application/json", "text/html", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.openxmlformats-officedocument.presentationml.presentation", "application/mspowerpoint", "application/vnd.ms-powerpoint", "application/vnd.ms-excel");
            // Validate file.
            if (!in_array(strtolower($mimeType), $allowedMimeTypes) || !in_array(strtolower($extension), $allowedExts)) {
                throw new \Exception("File does not meet the validation.");
            }

            $now = DateTime::createFromFormat('U.u', microtime(true));
            $time = str_replace([' ', ':', '-', '.'], '', $now->format("m-d-Y H:i:s.u"));
            // Generate new random name.
            $name = $nombre . "_" . $time . "." . $extension;
            $fullNamePath = realpath("") . "/uploads/" . $name;

            // Check server protocol and load resources accordingly.
            if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
                $protocol = "https://";
            } else {
                $protocol = "http://";
            }

            // Save file in the uploads folder.
            move_uploaded_file($tmpName, $fullNamePath);

            // Generate response.
            $response = new \StdClass;
            $response->location = $protocol . $_SERVER["HTTP_HOST"] . "/uploads/" . $name;

            // Send response.
            //return stripslashes(json_encode($response));

            return response()->json($response, 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                    JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            // Send error response.
            return $e->getMessage();
            http_response_code(404);
        }
    }
}

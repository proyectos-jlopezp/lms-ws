<?php

namespace App\Http\Controllers;

use App\Models\Contenido;
use App\Models\Periodo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ContenidoController extends Controller
{
    //

    public function getcontenido(Request  $request){
        

        $periodo = Periodo::where('estado',true)->with(['subperiodos'])->first();       
        $subPeriodos = $periodo->subperiodos;       
        $subPeriodoActivo = $subPeriodos->where('activo', true)->first();
       

        $contenido = Contenido::whereAulaCursoId($request->input("idAulaCurso"))->whereEstado(true)->whereSubperiodoId($subPeriodoActivo->id)->orderBy('created_at', 'asc')->get();
        return response()->json(['status' => true,
                'message' => 'Todos los registros',
                'body' => $contenido],
                200);
    }

    public function addcontenido(Request  $request)
    {



        try
        {
            $idAulaCurso = $request->idCursoAula;
            $idSubPeriodo = $request->idSubPeriodo;
            if($idSubPeriodo == 0){
                $periodo = Periodo::where('estado',true)->with(['subperiodos'])->first();       
                $subPeriodos = $periodo->subperiodos;       
                $subPeriodoActivo = $subPeriodos->where('activo', true)->first();
                $idSubPeriodo = $subPeriodoActivo->id;
            }
           
            
            
            Contenido::where(['aula_curso_id' => $idAulaCurso, 'subperiodo_id' => $idSubPeriodo])->update(['estado' => false]);
            
            $jsonString = json_encode($request->carpetas);
            $arr = json_decode($jsonString, true);
            
            for ($i = 0; $i < sizeof($arr); $i++) {

                
                
                
                if(!empty($arr[$i]['id'])){
                    $contenido = Contenido::whereId($arr[$i]['id'])->first();                   
                }else{
                    $contenido = new Contenido;
                }
                
                $contenido->aula_curso_id = $idAulaCurso;
                $contenido->estado = true;
                $contenido->subperiodo_id = $idSubPeriodo;
                $titulo = $arr[$i]['titulo'];
                $data = $arr[$i]['data'];

                $contenido->titulo = $titulo;
                $contenido->data = $data;
                
                $contenido->save();

            }

            
            
            return response()->json(['status' => true,
                'message' => 'Contenidos actualizados correctamente',
                'body' => $arr],
                200);

        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }

    public function deleteContenido(Request  $request){
        
        if($request->id)
            Contenido::where(['id' => $request->id])->update(['estado' => false]);

        
        return response()->json(['status' => true,
                'message' => 'Todos los registros'
                ],
                200);
    }

    public function actualizarCarpeta(Request  $request){
        
        if($request->id)
            Contenido::where(['id' => $request->id])->update(['titulo' => $request->nombreCarpeta]);

        
        return response()->json(['status' => true,
                'message' => 'Todos los registros'
                ],
                200);
    }

    public function addCarpeta(Request $request){
        $idAulaCurso = $request->idCursoAula;

        $idSubPeriodo = $request->idSubPeriodo;
        
        if($idSubPeriodo == 0){
            
            $periodo = Periodo::where('estado',true)->with(['subperiodos'])->first();       
            $subPeriodos = $periodo->subperiodos;       
            $subPeriodoActivo = $subPeriodos->where('activo', true)->first();
            $idSubPeriodo = $subPeriodoActivo->id;
        }



        $contenido = new Contenido();
        $contenido->titulo = $request->titulo;

        $contenido->aula_curso_id = $idAulaCurso;
        $contenido->estado = true;
        $contenido->subperiodo_id = $idSubPeriodo;
        $contenido->data = $request->data;
        
        $contenido->save();
    }

    public function getcontenidoBySubperiodo(Request  $request){

        $idSubPeriodo = $request->input("idSubPeriodo");
        if($idSubPeriodo == 0){
            
            $periodo = Periodo::where('estado',true)->with(['subperiodos'])->first();       
            $subPeriodos = $periodo->subperiodos;       
            $subPeriodoActivo = $subPeriodos->where('activo', true)->first();
            $idSubPeriodo = $subPeriodoActivo->id;
        }


        $contenido = Contenido::whereAulaCursoId($request->input("idAulaCurso"))->whereEstado(true)->whereSubperiodoId($idSubPeriodo)->orderBy('created_at', 'asc')->get();
        return response()->json(['status' => true,
                'message' => 'Todos los registros',
                'body' => $contenido],
                200);
    }
}

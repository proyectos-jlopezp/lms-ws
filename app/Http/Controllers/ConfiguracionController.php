<?php

namespace App\Http\Controllers;
use App\Models\Configuracion;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class ConfiguracionController extends Controller
{
    
    public function add(Request $request){
        try
        {
            $configuracion = new Configuracion;
            $configuracion->cPrincipal = $request->cPrincipal;
            $configuracion->cSecundario = $request->cSecundario;
            $configuracion->save();

            
            return response()->json(['status' => true,
                'message' => 'Area Registrada',
                'body' => $configuracion],
                200);
        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }

    public function all(){
        try
        {
            Log::info('Configuracion all');
            $records = Configuracion::all()->first();

            return response()->json(['status' => true,
                'message' => 'Resultados Encontrados',
                'body' => $records],
                200);
                
        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }

    public function edit(Request $request)
    {
        try {
            $data = json_decode($request->data);

            $configuracion = Configuracion::find($data->id);
            
            $configuracion->cPrincipal = ($data->cPrincipal != null) ? $data->cPrincipal : $configuracion->cPrincipal;
            $configuracion->cSecundario = ($data->cSecundario != null) ? $data->cSecundario : $configuracion->cSecundario;
            $configuracion->cSecundario = ($data->cSecundario != null) ? $data->cSecundario : $configuracion->cSecundario;
            

            //Imagen del usuario
            if ($request->hasFile('img')) {
                $file = $request->file('img');
                $path = $file->store('images');
                $configuracion->imagen = $path;
            }

            //Imagen del usuario
            if ($request->hasFile('img_login')) {
                $file = $request->file('img_login');
                $path = $file->store('images');
                $configuracion->imagen_login = $path;
            }

            $configuracion->save();



            return response()->json(
                [
                    'status' => true,
                    'message' => 'Inicie sesión nuevamente para observar los cambios realizados',
                    'body' => $configuracion
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }
}

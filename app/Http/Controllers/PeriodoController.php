<?php

namespace App\Http\Controllers;

use App\Models\Periodo;
use Illuminate\Http\Request;

use App\Utilities\ProxyRequest;

class PeriodoController extends Controller
{
    //

    protected $proxy;
    public function __construct(ProxyRequest $proxy)
    {
        $this->proxy = $proxy;
    }

    public function store(Request $request)
    {
        try
        {
            $id = $request->input('id');
            $record = Periodo::firstOrNew(['id' => $id]);
            $record->fill($request->all());
            $record->save();

            return response()->json(['status' => true,
                'message' => ($id) ? 'Periodo editado con éxito' : 'Periodo registrado con éxito',
                'body' => $record],
                200);

        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }

    public function all()
    {
        try
        {
            $periodos = Periodo::with(['subperiodos'])->get();
            return response()->json(['status' => true,
                'message' => 'Anios Encontrados',
                'body' => $periodos],
                200);
        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }

    public function delete($id)
    {
        try
        {
            $record = Periodo::find($id);
            $record->delete();
            return response()->json(['status' => true,
                'message' => 'Registro Eliminado'
                ],
                200);
        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }

    public function actual($id)
    {
        try
        {
            $periodo = Periodo::whereEstado(true)->first();
            if ($periodo) {
                $periodo->estado = false;
                $periodo->save();
            }

            $periodo = Periodo::find($id);
            $periodo->estado = true;
            $periodo->save();

            return response()->json(['status' => true,
                'message' => 'Periodo Actualizado',
                'body' => $periodo],
                200);
        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }

    public function getActual()
    {
        try
        {
            $periodo = Periodo::where('estado',true)->with(['subperiodos' => function ($q) {
                                                            $q->orderBy('secuencia', 'asc');
              }])->first();
            return response()->json(['status' => true,
                'message' => 'Periodo actual',
                'body' => $periodo],
                200);
        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }
}

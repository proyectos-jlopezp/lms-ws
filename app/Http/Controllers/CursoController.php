<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Curso;
use App\Models\Area;
use App\Models\Competencia;
use App\Models\Capacidad;
use Illuminate\Support\Facades\Log;

class CursoController extends Controller
{
    //
    public function all(Request $request)
    {
        try {

            $area_id = $request->input("idArea");
            $cursos = Curso::whereAreaId($area_id)->whereEstado(true)->get();

            for ($i = 0; $i < sizeof($cursos); $i++) {
                $competencias = Competencia::whereCursoId($cursos[$i]->id)->whereEstado(true)->get();

                for ($j = 0; $j < sizeof($competencias); $j++) {
                    $capacidades = Capacidad::whereCompetenciaId($competencias[$j]->id)->get();
                    $competencias[$j]->capacidades = $capacidades;
                }

                $cursos[$i]->competencias = $competencias;
            }


            return response()->json(
                [
                    'status' => true,
                    'message' => 'Cursos Encontrados',
                    'body' => $cursos
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function activos()
    {
        try {
            $cursos = Curso::whereEstado(true)->get();

            for ($i = 0; $i < sizeof($cursos); $i++) {
                $competencias = Competencia::whereCursoId($cursos[$i]->id)->whereEstado(true)->get();

                for ($j = 0; $j < sizeof($competencias); $j++) {
                    $capacidades = Capacidad::whereCompetenciaId($competencias[$j]->id)->get();
                    $competencias[$j]->capacidades = $capacidades;
                }

                $cursos[$i]->competencias = $competencias;
            }


            return response()->json(
                [
                    'status' => true,
                    'message' => 'Cursos Encontrados',
                    'body' => $cursos
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function aulas($id)
    {
        $curso = Curso::whereId($id)->with(['aulas.grado.nivel'])->first();

        return response()->json(
            [
                'status' => true,
                'message' => 'Alumno Encontrados',
                'body' => $curso
            ],
            200
        );
    }

    public function add(Request $formdata)
    {
        try {
            $request = json_decode($formdata->data);
            $cursoA = Curso::whereNombre($request->nombre)->where('area_id', $request->area_id)->get();
            $curso = new Curso;
            $curso->nombre = $request->nombre;
            $curso->estado = true;
            $curso->area_id = $request->area_id;
            if ($formdata->hasFile('img')) {
                $file = $formdata->file('img');
                $path = $file->store('images');
                $curso->imagen = $path;
            }
            
            if ($cursoA->isEmpty()) {
                $curso->save();
            } else {
                $curso->id = $cursoA[0]->id;
            }
            $jsonString = json_encode($request->competencias);
            $arr = json_decode($jsonString, true);

            for ($i = 0; $i < sizeof($arr); $i++) {

                $competenciaA = Competencia::whereCursoId($curso->id)->whereNombre($arr[$i]['nombre'])->get();

                $competencia = new Competencia();
                $competencia->curso_id = $curso->id;
                $competencia->nombre = $arr[$i]['nombre'];
                $competencia->estado = true;
                if ($competenciaA->isEmpty()) {
                    $competencia->save();
                } else {
                    $competencia->id = $competenciaA[0]->id;
                }

                //Eliminar capacidades
                Capacidad::where('competencia_id', $competencia->id)->delete();

                $capacidades = $arr[$i]['capacidades'];
                for ($j = 0; $j < sizeof($capacidades); $j++) {
                    $capacidad = new Capacidad();
                    $capacidad->competencia_id = $competencia->id;
                    $capacidad->nombre = $capacidades[$j]['nombre'];
                    $capacidad->estado = true;
                    $capacidad->save();
                }
            }


            return response()->json(
                [
                    'status' => true,
                    'message' => 'Curso registrado',
                    'body' => $curso
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function eliminar(Request $request)
    {
        try {
            $curso = Curso::find($request->id);
            //TODO: No permitir eliminar curso si es que existe un aula con un alumno registrado

            $curso->estado = false;
            $curso->save();

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Curso eliminado'
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function editar(Request $formdata)
    {
        try {
            $request = json_decode($formdata->data);
            $curso = Curso::where('id', $request->id)->first();

            $curso->nombre = $request->nombre;
            if ($formdata->hasFile('img')) {
                $file = $formdata->file('img');
                $path = $file->store('images');
                $curso->imagen = $path;
            }
            $curso->save();
            /*$jsonString = json_encode($request->competencias);
            $arr = json_decode($jsonString, true);

             /*Colocar todas las competencias con estado 0*/
            Competencia::whereCursoId($curso->id)->update(['estado' => 0]);


            foreach ($request->competencias as $competencia) {

                if (!empty($competencia->id)) {
                    $competenciaEdit = Competencia::whereId($competencia->id)->first();
                    /*Colocar todas las capacidades de la competencia con estado 0*/
                    Capacidad::where('competencia_id', $competencia->id)->update(['estado' => 0]);
                } else {
                    $competenciaEdit = new Competencia();
                    $competenciaEdit->curso_id = $curso->id;
                }
                $competenciaEdit->estado = true;
                $competenciaEdit->nombre = $competencia->nombre;
                $competenciaEdit->save();

                foreach ($competencia->capacidades as $capacidad) {
                    if (!empty($capacidad->id)) {
                        $capacidadEdit = Capacidad::whereId($capacidad->id)->first();
                    } else {
                        $capacidadEdit = new Capacidad();
                        $capacidadEdit->competencia_id = $competenciaEdit->id;
                    }
                    $capacidadEdit->nombre = $capacidad->nombre;
                    $capacidadEdit->estado = true;
                    $capacidadEdit->save();
                }
            }


            return response()->json(
                [
                    'status' => true,
                    'message' => 'Curso registrado',
                    'body' => $curso
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getTraceAsString()
                ],
                500
            );
        }
    }
}

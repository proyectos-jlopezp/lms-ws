<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Competencia;
use Illuminate\Support\Facades\Log;

class CompetenciaController extends Controller
{
    //
    public function all()
    {
        try
        {
            $competencias = Competencia::with(['curso'])->get();

            return response()->json(['status' => true,
                'message' => 'Competencias Encontrados',
                'body' => $competencias],
                200);
        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }
}

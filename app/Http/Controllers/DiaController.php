<?php

namespace App\Http\Controllers;
use App\Models\Dia;
use Illuminate\Http\Request;

class DiaController extends Controller
{
    //
    public function all()
    {
        try
        {
            $records = Dia::all();

            return response()->json(['status' => true,
                'message' => 'Resultados Encontrados',
                'body' => $records],
                200);
                
        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }
}

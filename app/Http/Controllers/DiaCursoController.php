<?php

namespace App\Http\Controllers;
use App\Models\DiaCurso;
use Illuminate\Http\Request;
use App\Http\Requests\DiaCursoRequest;

class DiaCursoController extends Controller
{
    //
    public function store(DiaCursoRequest $request)
    {
        try
        {
            $id = $request->input('id');
            $record = DiaCurso::firstOrNew(['id' => $id]);
            $record->fill($request->all());
            $record->save();
            
            return response()->json(['status' => true,
                'message' => ($id) ? 'DiaCurso editada con éxito' : 'DiaCurso registrada con éxito',
                'body' => $record],
                200);

        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }
}

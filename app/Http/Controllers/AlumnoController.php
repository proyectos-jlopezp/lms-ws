<?php

namespace App\Http\Controllers;

use App\Http\Requests\UsuarioRequest;
use App\Models\Apoderado;
use App\Models\TipoUsuario;
use App\Models\Usuario;
use App\Models\Estado;
use App\Models\Dia;
use App\Models\DiaCurso;
use App\Models\Curso;
use App\Models\AulaCurso;
use App\Models\AlumnoAula;
use App\Models\Aula;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;
use App\Jobs\ProcessMail;

use Illuminate\Support\Facades\Storage;


class AlumnoController extends UsuarioController
{
    //
    public function nuevos()
    {
        try {
            $records = Usuario::with(['estado'])->whereHas('tipo_usuario', function ($q) {
                $q->where('id', '=', TipoUsuario::whereNombre('ALUMNO')->first()->id);
            })->whereHas('estado', function ($q) {
                $q->where('id', '=', Estado::whereValor('NUEVO')->first()->id);
            })->get();

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Registros Encontrados',
                    'body' => $records
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function horario($id)
    {

        try {

            $record = AlumnoAula::whereAlumno_id($id)->with(['aula.cursos_aula', 'aula.periodo'  => function ($q) {
                $q->whereEstado(true);
            }])->first();

            $current_day = $this->getCurrentDay();

            $clases_semana = DiaCurso::whereIn('aula_curso_id', $record->aula->cursos_aula->map->only(['id']))->orderBy('hora_inicio', 'asc')->get();

            foreach ($clases_semana as $clase) {
                $this->setClaseHorario($current_day, $clase);
            }

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Horario',
                    'body' => $clases_semana
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error al obtener el horario del alumno.',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function miDia($id)
    {
        try {

            $record = AlumnoAula::whereAlumno_id($id)->with(['aula.cursos_aula', 'aula.periodo'  => function ($q) {
                $q->whereEstado(true);
            }])->first();

            if ($record == null) {

                return response()->json(
                    [
                        'status' => true,
                        'message' => 'No hay clases para este usuario',
                    ],
                    200
                );
            }

            $current_day = date('w');
            if ($current_day == 0)
                $current_day = 7;
            $dia = Dia::find($current_day);
            //$midia = $alumno->aulas_alumno;
            $midia = DiaCurso::with(['aula_curso.aula.grado', 'aula_curso.aula.nivel', 'aula_curso.curso'])
                ->where('dia_id', '=', $dia->id)->whereIn('aula_curso_id', $record->aula->cursos_aula->map->only(['id']))->orderBy('hora_inicio', 'asc')
                ->get();

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Mi dia',
                    'body' => $midia
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error al obtener el dia del profesor.',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function all()
    {
        try {
            /*Retornar solo los usuarios tipo profesor y con el valor de su estado */
            $alumnos = Usuario::with(['estado', 'apoderado'])->whereHas('tipo_usuario', function ($q) {
                $q->where('id', '=', TipoUsuario::whereNombre('ALUMNO')->first()->id);
            })->get()->each(function ($user) { /*Ignorar los campos innecesarios */
                $user->makeHidden(['estado_id', 'apoderado_id', 'tipo_usuario_id', 'imagen', 'created_at', 'updated_at', 'fecha_nacimiento']);
            });;
            return response()->json(
                [
                    'status' => true,
                    'message' => 'Alumnos Encontrados',
                    'body' => $alumnos
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function cursos($id)
    {
        try {
            $record = AlumnoAula::whereAlumno_id($id)->with(['aula.cursos', 'aula.periodo'  => function ($q) {
                $q->whereEstado(true);
            }])->first();


            return response()->json(
                [
                    'status' => true,
                    'message' => 'Registros Encontrados',
                    'body' => $record->aula->cursos
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function participantes($id)
    {
        try {
            $record = AlumnoAula::whereAlumno_id($id)->with(['aula.cursos', 'aula.periodo'  => function ($q) {
                $q->whereEstado(true);
            }])->first();

            $records = AlumnoAula::whereAula_id($record->aula_id)->with(['alumno'])->get();

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Registros Encontrados',
                    'body' => $records
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function getApoderado($dni)
    {
        try {
            $apoderado = Apoderado::whereDni($dni)->first();

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Apoderado encontrado',
                    'body' => $apoderado
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }



    public function edit(Request $formRequest)
    {
        try {
            $request = json_decode($formRequest->data);
            $apoderado = Apoderado::find($request->apoderado->id);
            $this->fillValuesApoderado($request, $apoderado);
            $apoderado->save();

            $alumno = Usuario::find($request->id);
            $this->fillValuesAlumno($request, $alumno);

            //Imagen del usuario
            if ($formRequest->hasFile('img')) {
                $alumno->imagen = $this->setProfileImage($formRequest, 'img', 'profile' . $alumno->dni);
            }

            $alumno->save();

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Alumno guardado'
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function delete($id)
    {
        try {
            $alumno = Usuario::whereId($id)->first();
            if ($alumno == null) {
                return response()->json(
                    [
                        'status' => false,
                        'message' => 'Alumnno no existe.',

                    ],
                    200
                );
            } else
                //Logica de verificacion en caso el alumno.. tenga clases registradas            
                if (count($alumno->aulas_alumno) > 0) {
                    return response()->json(
                        [
                            'status' => false,
                            'message' => 'No puede eliminarse el alumno porque ha tenido aulas asignadas.',

                        ],
                        200
                    );
                } else {
                    $alumno->delete();
                }

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Se elimino el alumno.',
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }



    public function add(UsuarioRequest $request)
    {
        try {
            $data = json_decode($request->data);
            $apoderado_valid = $this->validateApoderado($data->apoderado->dni);
            if ($apoderado_valid != null)
                $apoderado = $apoderado_valid;
            else
                $apoderado = new Apoderado;

            $this->fillValuesApoderado($data, $apoderado);
            $apoderado->save();

            $alumno = new Usuario;
            $this->fillValuesAlumno($data, $alumno);
            $alumno->apoderado_id =  $apoderado->id;
            $user_password = $this->generatePassword();
            Log::info('alumno_password:' . $user_password);
            $alumno->password = bcrypt($user_password);

            //Imagen del usuario
            if ($request->hasFile('img')) {
                $alumno->imagen = $this->setProfileImage($request, 'img', 'profile' . $alumno->dni);
            }
            $alumno->save();
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }

        //Envio de correo a través de JOBS        
        ProcessMail::dispatch($alumno, $user_password);
    }


    private function fillValuesApoderado($request, Apoderado $apoderado)
    {
        $apoderado->dni = $request->apoderado->dni;
        $apoderado->apellido_paterno = $request->apoderado->apellido_paterno;
        $apoderado->apellido_materno = $request->apoderado->apellido_materno;
        $apoderado->nombre = $request->apoderado->nombre;
        $apoderado->celular = $request->apoderado->celular;
        $apoderado->telefono = $request->apoderado->telefono;
        $apoderado->correo = $request->apoderado->correo;
    }
    private function fillValuesAlumno($request, Usuario $alumno)
    {
        $alumno->dni = $request->dni;
        $alumno->apellido_paterno = $request->apellido_paterno;
        $alumno->apellido_materno = $request->apellido_materno;
        $alumno->nombre = $request->nombre;
        $alumno->direccion = $request->direccion;
        //$alumno->telefono = $request->telefono;

        $propiedad_existe = property_exists($request, 'celular');
        $alumno->celular = $propiedad_existe == null ? "" : $request->celular;

        $alumno->correo = $request->correo;
        //$alumno->imagen = "";
        /*  */
        //$alumno->password = bcrypt("1234567");
        /*  */
        $s = $request->fecha_nacimiento;
        $date = str_replace('/', '-', $s);
        $date = strtotime($date);
        $alumno->fecha_nacimiento =  date('Y-m-d', $date);
        $alumno->tipo_usuario_id = $request->tipo_usuario_id;

        $alumno->estado_id = $request->estado_id;
    }
    private function validateApoderado($dni)
    {
        return Apoderado::where('dni', $dni)->first();
    }

    public function validateImport()
    {
        $estados = ['NUEVO', 'NO MATRICULADO'];
        try {
            $records = Usuario::with(['estado'])->whereHas('tipo_usuario', function ($q) {
                $q->where('id', '=', TipoUsuario::whereNombre('ALUMNO')->first()->id);
            })->whereHas('estado', function ($q) use ($estados) {
                $q->whereIn('id', Estado::whereIn('valor', $estados)->get()->map->only(['id']));
            })->get();

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Registros Encontrados',
                    'body' => $records
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }
}

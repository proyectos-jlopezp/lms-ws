<?php

namespace App\Http\Controllers;
use App\Models\Nivel;
use Illuminate\Http\Request;

class NivelController extends Controller
{
    //
    public function all()
    {
        try
        {
            $records = Nivel::all();

            return response()->json(['status' => true,
                'message' => 'Resultados Encontrados',
                'body' => $records],
                200);
                
        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }
}

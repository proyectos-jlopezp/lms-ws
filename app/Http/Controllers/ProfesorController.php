<?php

namespace App\Http\Controllers;

use App\Http\Requests\UsuarioRequest;
use App\Models\TipoUsuario;
use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\DiaCurso;
use App\Models\Dia;
use App\Jobs\ProcessMail;
use App\Models\Aula;


use Illuminate\Support\Facades\DB;

class ProfesorController extends UsuarioController
{
    //
    public function all()
    {
        try {
            /*Retornar solo los usuarios tipo profesor y con el valor de su estado */
            $profesores = Usuario::with(['estado'])->whereHas('tipo_usuario', function ($q) {
                $q->where('id', '=', TipoUsuario::whereNombre('PROFESOR')->first()->id);
            })->get()->each(function ($user) { /*Ignorar los campos innecesarios */
                $user->makeHidden(['estado_id', 'apoderado_id', 'tipo_usuario_id', 'imagen', 'created_at', 'updated_at', 'fecha_nacimiento']);
            });;

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Profesores Encontrados',
                    'body' => $profesores
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function activos()
    {
        try {
            /*Retornar solo los usuarios tipo profesor y con el valor de su estado */
            $profesores = Usuario::with(['estado'])->whereHas('tipo_usuario', function ($q) {
                $q->where('id', '=', TipoUsuario::whereNombre('PROFESOR')->first()->id);
            })->whereEstado_id(5)->get()->each(function ($user) { /*Ignorar los campos innecesarios */
                $user->makeHidden(['estado_id', 'apoderado_id', 'tipo_usuario_id', 'imagen', 'created_at', 'updated_at', 'fecha_nacimiento']);
            });;

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Profesores Encontrados',
                    'body' => $profesores
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }



    public function edit(Request $request)
    {
        try {
            $data = json_decode($request->data);
            Log::info('soy el usuario:' . $data->id);
            $profesor = Usuario::find($data->id);
            $this->fillValuesProfesor($request, $profesor);
            $profesor->save();

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Alumno guardado'
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function aulas($id)
    {
        $profesor = Usuario::whereId($id)->first();

        return response()->json(
            [
                'status' => true,
                'message' => 'Alumno Encontrados',
                'body' => $profesor->aulas_profesor
            ],
            200
        );
    }

    public function delete($id)
    {
        try {
            $profesor = Usuario::whereId($id)->first();
            if ($profesor == null) {
                return response()->json(
                    [
                        'status' => false,
                        'message' => 'Profesor no existe.',

                    ],
                    200
                );
            } else
                //Logica de verificacion en caso el profesor.. tenga clases registradas            
                if (count($profesor->aulas_profesor) > 0) {
                    return response()->json(
                        [
                            'status' => false,
                            'message' => 'No puede eliminarse el profesor porque ha tenido aulas asignadas.',

                        ],
                        200
                    );
                } else {
                    $profesor->delete();
                }

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Se elimino el profesor.',
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }



    public function add(UsuarioRequest $request)
    {

        /*Creacion del usuario profesor */
        try {
            $profesor = new Usuario;
            $this->fillValuesProfesor($request, $profesor);
            $user_password = $this->generatePassword();
            $profesor->password = bcrypt($user_password);
            /*  */

            $profesor->tipo_usuario_id = TipoUsuario::whereNombre('PROFESOR')->first()->id;
            $profesor->save();
        } catch (\Exception $e) {
            Log::info('estoy here excp');
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error al crear el usuario profesor.',
                    'body' => $e->getMessage()
                ],
                500
            );
        }

        //Envio de correo a través de JOBS        
        ProcessMail::dispatch($profesor, $user_password);
    }


    private function fillValuesProfesor(Request $request, Usuario $usuario)
    {
        //Datos del usuario
        $data = json_decode($request->data);
        $usuario->dni = $data->dni;
        $usuario->apellido_paterno = $data->apellido_paterno;
        $usuario->apellido_materno = $data->apellido_materno;
        $usuario->nombre = $data->nombre;
        $usuario->direccion = $data->direccion;
        $usuario->telefono = $data->telefono;
        $usuario->celular = $data->celular;
        $usuario->correo = $data->correo;
        $usuario->fecha_nacimiento = $data->fecha_nacimiento;
        $s = $usuario->fecha_nacimiento;
        $date = str_replace('/', '-', $s);
        $date = strtotime($date);
        $usuario->fecha_nacimiento =  date('Y-m-d', $date);
        $usuario->estado_id = $data->estado_id;

        //Imagen del usuario
        if ($request->hasFile('img')) {
            $usuario->imagen = $this->setProfileImage($request, 'img', 'profile' . $usuario->dni);
        }
    }

    public function horario($id)
    {

        try {
            $profesor = Usuario::find($id);
            $current_day = $this->getCurrentDay();
            $clases_semana = DiaCurso::whereIn('aula_curso_id', $profesor->aulas_profesor->map->only(['id']))->orderBy('hora_inicio', 'asc')->get();
            foreach ($clases_semana as $clase) {
                $this->setClaseHorario($current_day, $clase);
            }

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Horario',
                    'body' => $clases_semana
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error al obtener el horario del profesor.',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function miDia($id)
    {
        try {
            $profesor = Usuario::find($id);
            $current_day = date('w');
            if ($current_day == 0)
                $current_day = 7;
            $dia = Dia::find($current_day);
            $midia = DiaCurso::with(['aula_curso.aula.grado', 'aula_curso.aula.nivel', 'aula_curso.curso'])->where('dia_id', '=', $dia->id)->whereIn('aula_curso_id', $profesor->aulas_profesor->map->only(['id']))->orderBy('hora_inicio', 'asc')->get();

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Mi dia',
                    'body' => $midia
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error al obtener el dia del profesor.',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    function cmpArrayGrados($a, $b)
    {
        return strcmp($a["title"], $b["title"]);
    }

    public function grados(Request $request)
    {


        /*
            select * from aula au
            inner join aula_curso auCu on auCu.aula_id = au.id
            inner join aula_curso_profesor auCuPro on auCuPro.aula_curso_id = auCu.id
            where auCuPro.profesor_id='2'
        */

        $grados = DB::table('aula')             
             ->select('grado.valor as grado', 'aula.seccion as nombre' , 'nivel.nombre as nivel', 'aula.id as id')
             ->join('aula_curso', 'aula_curso.aula_id', '=', 'aula.id')
             ->join('aula_curso_profesor', 'aula_curso_profesor.aula_curso_id', '=', 'aula_curso.id')
             ->join('nivel', 'nivel.id', '=', 'aula.nivel_id')
             ->join('grado', 'grado.id', '=', 'aula.grado_id')
             ->where('aula_curso_profesor.profesor_id', $request->input("idProfesor"))
             ->groupBy('aula.id', 'grado.valor', 'nivel.nombre', 'nivel.id')
             ->orderBy('nivel.id', 'asc')
             ->orderBy('grado.valor', 'asc')
             ->get();
        
        foreach ($grados as $grado) {


            /*
                select * from curso cu
                inner join aula_curso auCu on auCu.curso_id = cu.id
                inner join aula_curso_profesor auCuPro on auCuPro.aula_curso_id = auCu.id
                where auCu.aula_id = '2' and auCuPro.profesor_id='2'
            */
            $grado->nombre = $grado->grado. '° '.$grado->nombre. ' - '. $grado->nivel;
            $cursos = DB::table('curso')
                ->select('curso.nombre as nombre', 'aula_curso.id as idAulaCurso')
                ->join('aula_curso', 'aula_curso.curso_id', '=', 'curso.id')
                ->join('aula_curso_profesor', 'aula_curso_profesor.aula_curso_id', '=', 'aula_curso.id')
                ->where('aula_curso_profesor.profesor_id', $request->input("idProfesor"))
                ->where('aula_curso.aula_id', $grado->id)
                ->get();

            $grado->cursos = $cursos;
        }




        return response()->json(
            [
                'status' => true,
                'message' => 'Datos de cursos',
                'body' => $grados
            ],
            200
        );
    }
}

<?php

namespace App\Http\Controllers;
use App\Models\AulaCurso;
use App\Models\Contenido;

use Illuminate\Http\Request;
use App\Http\Requests\AulaCursoRequest;
class AulaCursoController extends Controller
{
    //
    public function store(AulaCursoRequest $request)
    {
        try
        {
            $id = $request->input('id');
            $record = AulaCurso::firstOrNew(['id' => $id]);
            $record->fill($request->all());
            $record->save();
            
            return response()->json(['status' => true,
                'message' => ($id) ? 'AulaCurso editada con éxito' : 'AulaCurso registrada con éxito',
                'body' => $record],
                200);

        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }

   
}

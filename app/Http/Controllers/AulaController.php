<?php

namespace App\Http\Controllers;

use App\Models\Aula;
use App\Models\AulaCurso;
use Illuminate\Http\Request;
use App\Http\Requests\AulaRequest;
use App\Models\DiaCurso;
use App\Models\AulaCursoProfesor;
use App\Models\Usuario;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Arr;
use \Datetime;

class AulaController extends Controller
{
    //
    public function store(AulaRequest $request)
    {
        try {
            $id = $request->input('id');
            $record = Aula::firstOrNew(['id' => $id]);
            $record->fill($request->all());
            $record->save();

            return response()->json(
                [
                    'status' => true,
                    'message' => ($id) ? 'Aula editada con exito' : 'Aula registrada con exito',
                    'body' => $record
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function all()
    {
        try {
            $records = Aula::with(['grado', 'nivel', 'alumnos', 'cursos'])->get();

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Registros econtrados',
                    'body' => $records
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function find($id)
    {
        try {
            $record = Aula::with(['grado', 'nivel', 'alumnos', 'cursos_aula.curso', 'cursos_aula.profesores', 'cursos_aula.horarios'])->find($id);

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Registro econtrado',
                    'body' => $record
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function delete($id)
    {
        try {
            $aula = Aula::with(['alumnos'])->find($id);
            Aula::find($id)->delete();
            foreach ($aula->alumnos as $alumno) {
                $user = Usuario::find($alumno->id);
                $user->estado_id = 2;
                $user->save();
            }

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Registro eliminado'
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function storeCursos(Request $request)
    {
        try {

            //Primero se intenta eliminar cursos.. luego se registran los nuevos
            foreach ($request->remove as $curso) {
                $aula_curso = AulaCurso::find($curso['id'])->delete();
            }


            foreach ($request->cursos as $curso) {

                foreach ($curso['remove'] as $horario) {
                    $dia_curso = DiaCurso::find($horario['id'])->delete();
                }

                $aula_curso = AulaCurso::firstOrNew(['id' => $curso['id']]);
                // $aula_curso = new AulaCurso;
                $aula_curso->aula_id = $request->aula_id;
                $aula_curso->curso_id = $curso['curso_id'];
                $aula_curso->save();
                // $aula_curso_profesor = new AulaCursoProfesor;
                $aula_curso_profesor = AulaCursoProfesor::firstOrNew(['id' => $curso['aula_curso_profesor_id']]);
                $aula_curso_profesor->aula_curso_id = $aula_curso->id;
                $aula_curso_profesor->profesor_id = $curso['profesor_id'];
                $aula_curso_profesor->save();
                foreach ($curso['horarios'] as $horario) {
                    // if ($horario['id']) continue;
                    $dia_curso = DiaCurso::firstOrNew(['id' => $horario['id']]);
                    // $dia_curso = new DiaCurso;
                    $dia_curso->dia_id = $horario['dia_id'];
                    $dia_curso->hora_inicio = $horario['hora_inicio'];
                    $dia_curso->hora_fin = $horario['hora_fin'];
                    $dia_curso->aula_curso_id = $aula_curso->id;
                    $dia_curso->save();
                }
            }


            return response()->json(
                [
                    'status' => true,
                    'message' => 'Registros actualizados'
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function validateSchedule(Request $request)
    {

        /* * 
         * POSTMAN
         * 
          {
                "profesor_id":22,
                "periodo_id":5,
                "horarios":[
                    {"dia_id":1,"hora_inicio":"08:00","hora_fin":"10:00"},
                    {"dia_id":2,"hora_inicio":"10:00","hora_fin":"12:00"},
                    {"dia_id":3,"hora_inicio":"13:00","hora_fin":"16:00"}
                ]
            }
        */
        try {
            $profesor_id = $request->profesor_id;
            $periodo_id = $request->periodo_id;
            $validate_horarios = $request->horarios;
            $conflics = [];

            $profesor_horarios = DiaCurso::where('periodo_id', '=', $periodo_id)->where('profesor_id', '=', $profesor_id)->get();

            foreach ($validate_horarios as $currentSchedule) {

                $diasHorario = $profesor_horarios->filter(function ($item) use ($currentSchedule) {
                    return $item->dia_id == $currentSchedule['dia_id'];
                });
                foreach ($diasHorario as $horario) {
                    $hora_inicio = $horario['hora_inicio'];
                    $hora_fin = $horario['hora_fin'];

                    $start_time1 = new DateTime($hora_inicio);
                    $end_time1 = new DateTime($hora_fin);

                    $start_time2 = new DateTime($currentSchedule['hora_inicio']);
                    $end_time2 = new DateTime($currentSchedule['hora_fin']);

                    $val1 = ($start_time2 >= $start_time1) && ($start_time2 < $end_time1);
                    $val2 = ($end_time2 > $start_time1) && ($end_time2 <= $end_time1);
                    if (($val1 || $val2) == 1) {
                        array_push($conflics, $horario);
                    }
                }
            }
            return response()->json(
                [
                    'status' => true,
                    'message' => 'Validacion satisfactoria',
                    'body' => $conflics
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }
}

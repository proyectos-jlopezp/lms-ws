<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AlumnoAula;
use App\Models\Usuario;

class AlumnoAulaController extends Controller
{
    public function store(Request $request)
    {
        try {
            $id = $request->input('id');
            $record = AlumnoAula::firstOrNew(['id' => $id]);
            $record->fill($request->all());
            $record->save();

            return response()->json(
                [
                    'status' => true,
                    'message' => ($id) ? 'Registro editado con éxito' : 'Registro agregado con éxito',
                    'body' => $record
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function addBatch(Request $request)
    {
        try {
            AlumnoAula::whereIn('id', array_column($request->removed, 'id'))->delete();

            AlumnoAula::insert($request->body);
            Usuario::whereIn('id', array_column($request->body, 'alumno_id'))->update(["estado_id" => 1]);
            Usuario::whereIn('id', array_column($request->removed, 'alumno_id'))->update(["estado_id" => 2]);
            // foreach ($request->removed as $record) {
            //     delete($record->id,$record->alumno_id);
            // }

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Registros agregados',
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function delete($id, $alumno_id)
    {
        $record = AlumnoAula::find($id);
        $record->delete();
        Usuario::whereIn('id', array_column($request->body, $alumno_id))->update(["estado_id" => 7]);
    }
}

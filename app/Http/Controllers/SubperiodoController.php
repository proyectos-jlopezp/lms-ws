<?php

namespace App\Http\Controllers;

use App\Models\Periodo;
use App\Models\Modalidad;
use App\Models\Subperiodo;
use Illuminate\Http\Request;

class SubperiodoController extends Controller
{
    //
    public function actual($id)
    {
        try
        {
            
            $record = Subperiodo::whereActivo(true)->first();
            if ($record) {
                $record->activo = false;
                $record->save();
            }

            $record = Subperiodo::find($id);
            $record->activo = true;
            $record->save();

            return response()->json(['status' => true,
                'message' => 'Subperiodo Actualizado',
                'body' => $record],
                200);

        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }

    public function add(Request $request)
    {
        try
        {
            $periodo = new Periodo;
            $periodo->nombre = $request->nombre;
            $periodo->modalidad = $request->modalidad;
            $periodo->inicio = $request->inicio;
            $periodo->estado = false;
            $periodo->save();

            for ($i = 1; $i <= $request->secuencia; $i++) {
                $subperiodo = new Subperiodo();
                $subperiodo->periodo_id = $periodo->id;
                $subperiodo->secuencia = $i;
                $subperiodo->save();
            }
            $periodo->subperiodos();
            return response()->json(['status' => true,
                'message' => 'Periodos Creados',
                'body' => $periodo->subperiodos],
                200);
        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }

    public function all()
    {
        Log::info('entra periodo all');
        try
        {
            $subperiodos = Subperiodo::with(['modalidad', 'periodo'])->get();

            return response()->json(['status' => true,
                'message' => 'Periodos Encontrados',
                'body' => $subperiodos],
                200);
        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }
}

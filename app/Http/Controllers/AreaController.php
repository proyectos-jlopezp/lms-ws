<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Area;

class AreaController extends Controller
{
    //
    public function all()
    {
        try
        {
            
            $areas = Area::whereEstado('1')->with(['cursos'])->get();

            return response()->json(['status' => true,
                'message' => 'Areas Encontradas',
                'body' => $areas],
                200);
        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }

    public function add(Request $request)
    {
        try
        {
            $area = new Area;
            $area->nombre = $request->nombre;
            $area->estado = true;
            $area->save();

            
            return response()->json(['status' => true,
                'message' => 'Area Registrada',
                'body' => $area],
                200);
        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }

    public function edit(Request $request)
    {
        try {
            $area = Area::find($request->id);
            
            $area->nombre = ($request->nombre != null) ? $request->nombre : $area->nombre;
            $area->estado = ($request->estado != null) ? $request->estado : $area->estado;
            
            $area->save();

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Área actualizada'
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }
}

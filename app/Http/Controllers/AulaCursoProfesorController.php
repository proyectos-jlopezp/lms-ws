<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AulaCursoProfesor;

class AulaCursoProfesorController extends Controller
{
    public function store(Request $request)
    {
        try
        {
            $id = $request->input('id');
            $record = AulaCursoProfesor::firstOrNew(['id' => $id]);
            $record->fill($request->all());
            $record->save();
            
            return response()->json(['status' => true,
                'message' => ($id) ? 'Registro editado con éxito' : 'Registro agregado con éxito',
                'body' => $record],
                200);

        } catch (\Exception $e) {
            return response()->json(['status' => false,
                'message' => 'Hubo un error',
                'body' => $e->getMessage()],
                500);
        }
    }

}

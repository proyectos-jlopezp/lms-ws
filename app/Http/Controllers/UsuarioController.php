<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Illuminate\Support\Facades\Log;
use App\Models\AulaCurso;
use App\Models\Curso;
use App\Models\Aula;
use App\Jobs\ProcessMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;



class UsuarioController extends Controller
{

    public function resetPassword(Request $request)
    {

        $usuario = Usuario::find($request->id);
        $user_password = $this->generatePassword();
        $usuario->password = bcrypt($user_password);
        $usuario->firstLogin = false;
        $usuario->save();
        ProcessMail::dispatch($usuario, $user_password);
        return response()->json(
            [
                'status' => true,
                'message' => 'Se restableció la contraseña. Correo enviado al usuario.',
            ],
            200
        );
    }

    //Retornar un usuario de cualquier tipo en base al id
    public function one($id)
    {
        try {
            // $anios = Anio::with('periodos')->get();
            $usuario = Usuario::whereId($id)->with(['tipo_usuario', 'estado', 'apoderado'])->first();
            return response()->json(
                [
                    'status' => true,
                    'message' => 'Usuario encontrado',
                    'body' => $usuario
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Hubo un error',
                    'body' => $e->getMessage()
                ],
                500
            );
        }
    }

    //Funcion que guarda la imagen de perfil del usuario y retorna el path en caso de exito
    public function setProfileImage($request, $field, $imgName)
    {

        if ($request->hasFile($field)) {


            if (file_exists(public_path() . '/images/' . $imgName)) {

                File::delete(public_path() . '/images/' . $imgName);
            }

            $file = $request->file($field);
            $path = $file->storeAs(
                'images',
                $imgName //. '.' . $file->extension()
            );

            return $path;
        } else {
            return null;
        }
    }


    public function generatePassword()
    {
        $pass_length = 8;
        $new_pass =  substr(str_shuffle("123456789"), 0, $pass_length);
        Log::info('nueva password:' . $new_pass);
        return $new_pass;
    }

    public function updatePassword(Request $request)
    {
        $record = Usuario::find($request->id);
        $record->password = bcrypt($request->password);
        $record->firstLogin = true;
        $record->save();
        return response()->json(
            [
                'status' => true,
                'message' => 'Contraseña actualizada'
            ],
            200
        );
    }

    public function getCurrentDay()
    {
        $current_day = date('w') - 1;
        if ($current_day == -1)
            $current_day = 6;
        return $current_day;
    }

    public function setClaseHorario($current_day, $clase)
    {
        $week_start = date('Y-m-d', strtotime('-' . $current_day . ' days'));
        $days_increment = $clase->dia_id - 1;
        $dia_clase = date('Y-m-d', strtotime($week_start . ' + ' . strval($days_increment) . ' days'));
        $aulaCurso = AulaCurso::whereId($clase->aula_curso_id)->first();
        $curso  = Curso::whereId($aulaCurso->curso_id)->first();
        $aula = Aula::whereId($aulaCurso->aula_id)->with(['grado', 'nivel'])->first();
        $today = date("Y-m-d");
        $difference_in_seconds = strtotime($today . " " . $clase->hora_fin) - strtotime($today . " " . $clase->hora_inicio);
        $aula_format = $aula->grado->valor . "° " . $aula->seccion . " - " . $aula->nivel->nombre;

        //Se agregan los siguientes campos que serán usados en el calendario
        $clase->title = $curso->nombre;
        $clase->nombre_aula = $aula_format;
        $clase->duration = $difference_in_seconds / 60;
        $clase->date = $dia_clase;
    }
}

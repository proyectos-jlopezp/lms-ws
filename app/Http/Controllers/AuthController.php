<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Utilities\ProxyRequest;

use App\Models\Usuario;
use App\Models\Estado;


use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{


    protected $proxy;
    public function __construct(ProxyRequest $proxy)
    {
        $this->proxy = $proxy;
    }

    public function register()
    {
        $this->validate(request(), [
            'name' => 'required',
            'dni' => 'required',
            'password' => 'required',
        ]);

        $user = Usuario::create([
            'name' => request('name'),
            'dni' => request('dni'),
            'password' => bcrypt(request('password')),
        ]);

        $resp = $this->proxy->grantPasswordToken(
            $user->dni,
            request('password')
        );

        return response([
            'token' => $resp->access_token,
            'expiresIn' => $resp->expires_in,
            'message' => 'Your account has been created',
        ], 201);
    }

    /*public function test(){
        return 'Hello World 3';
    }*/


    public function login()
    {
        $user = Usuario::where('dni', request('dni'))->with('tipo_usuario')->first();
        if (!$user) {
            return response([
                'token' => '',
                'expiresIn' => '',
                'status' => false,
                'message' => 'No exite usuario registrado con ese DNI.',
            ], 200);
        }

        if ($user->estado_id) {
            $estado = Estado::whereId($user->estado_id)->first();
            if (in_array($estado->valor, ['SUSPENDIDO', 'RETIRADO', 'EXPULSADO', 'NO ACTIVO'])) {

                return response([
                    'token' => '',
                    'expiresIn' => '',
                    'status' => false,
                    'message' => 'El usuario no está habilitado para ingresar a la plataforma.',
                ], 200);
            }
        }

        if (!\Hash::check(request('password'), $user->password)) {
            return response([
                'token' => '',
                'expiresIn' => '',
                'status' => false,
                'message' => 'Combinación de DNI y contraseña incorrecta.',
            ], 200);
        }

        $resp = $this->proxy
            ->grantPasswordToken(request('dni'), request('password'));

        return response([
            'token' => $resp->access_token,
            'expiresIn' => $resp->expires_in,
            'status' => true,
            'message' => 'Login exitoso',
            'body' => $user,
        ], 200);
    }


    public function refreshToken()
    {
        $resp = $this->proxy->refreshAccessToken();
        return response([
            'token' => $resp->access_token,
            'expiresIn' => $resp->expires_in,
            'message' => 'Token ha sido refrescado',
        ], 200);
    }

    public function logout()
    {
        // remove the httponly cookie
        cookie()->queue(cookie()->forget('refresh_token'));

        return response([
            'message' => 'Cerrar sesion exitoso',
        ], 200);
    }
}

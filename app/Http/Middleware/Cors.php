<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Log;
use Closure;

class Cors
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $requestOrigin = $request->headers->get('origin');    

        $headerAuth = $request->headers->get('Authorization');
        if(!$headerAuth){
            return response([
                'token' => '',
                'expiresIn' => '',
                'status' => false,
                'message' => 'No nos hackees',
            ], 401)
            ->header('Access-Control-Allow-Origin', $requestOrigin)
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
            ->header('Access-Control-Allow-Credentials', 'true')
            ->header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
        }

        if (!str_contains($headerAuth, 'refresh_token')) { 
            return response([
                'token' => '',
                'expiresIn' => '',
                'status' => false,
                'message' => 'No tienes token',
            ], 401)
            ->header('Access-Control-Allow-Origin', $requestOrigin)
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
            ->header('Access-Control-Allow-Credentials', 'true')
            ->header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
        }

        return $next($request)
            ->header('Access-Control-Allow-Origin', $requestOrigin)
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
            ->header('Access-Control-Allow-Credentials', 'true')
            ->header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
        
    }
}

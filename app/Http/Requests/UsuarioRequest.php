<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;


class UsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function max_size_allowed()
    {
        return 5;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() //TODO: Cuando se edite usuario, no validar el DNI
    {
        $rules = [
            //'json.dni' => 'bail|required|unique:usuario,dni',
            'json.dni' => ['bail', 'required',  Rule::unique('usuario', 'dni')->where(function ($query) {
                return $query->whereNull('deleted_at');
            })],
            'json.estado_id' => 'bail|required|exists:estado,id',
            'img' => 'sometimes | image |max:' . ($this->max_size_allowed() * 1024)
        ];



        return $rules;
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'json.dni' => 'DNI',
            'json.estado_id' => 'estado',
            'img' => 'imagen'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        $customMessages = [
            'required' => 'El campo :attribute es obligatorio.',
            'unique' => 'El :attribute ingresado ya está registrado',
            'exists' => 'El :attribute ingresado no existe en el sistema',
            'img.max' => 'La imagen supera el tamaño máximo permitido de ' . $this->max_size_allowed() . ' mb'
        ];

        return $customMessages;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        //Log::info('data por validar:' . $this->data);
        $this->merge([
            'json' => json_decode($this->data, TRUE),
        ]);
    }
}

<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AlumnoAulaController;
use App\Http\Controllers\AlumnoController;
use App\Http\Controllers\AreaController;
use App\Http\Controllers\AulaController;
use App\Http\Controllers\AulaCursoProfesorController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CapacidadController;
use App\Http\Controllers\CompetenciaController;
use App\Http\Controllers\ContenidoController;
use App\Http\Controllers\ConfiguracionController;
use App\Http\Controllers\CursoController;
use App\Http\Controllers\DiaController;
use App\Http\Controllers\DiaCursoController;
use App\Http\Controllers\GradoController;
use App\Http\Controllers\NivelController;
use App\Http\Controllers\PeriodoController;
use App\Http\Controllers\ProfesorController;
use App\Http\Controllers\SubperiodoController;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\UsuarioController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
return $request->user();
});*/

Route::middleware('guest', 'throttle:3,1')->group(function () {
    Route::post('login', [AuthController::class, 'login']); 
    

    Route::prefix('configuracion')->group(function () {
        Route::get('all', [ConfiguracionController::class, 'all']);
    });
});

Route::middleware('user')->group(function () {
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh-token', [AuthController::class, 'refreshToken']);
    
    Route::prefix('periodo')->group(function () {
        Route::get('actual/{id}', [SubperiodoController::class, 'actual']);
        Route::post('add', [SubperiodoController::class, 'add']);
        Route::get('all', [SubperiodoController::class, 'all']);
    });
    
    Route::prefix('usuario')->group(function () {
        Route::post('password', [UsuarioController::class, 'updatePassword']);
        Route::post('resetPassword', [UsuarioController::class, 'resetPassword']);
        Route::get('one/{id}', [UsuarioController::class, 'one']);
    });
    
    Route::prefix('anio')->group(function () {
        Route::get('all', [PeriodoController::class, 'all']);
        Route::get('actual/{id}', [PeriodoController::class, 'actual']);
        Route::get('delete/{id}', [PeriodoController::class, 'delete']);
        Route::post('', [PeriodoController::class, 'store']);
        Route::get('getActual', [PeriodoController::class, 'getActual']);
    });
    
    Route::prefix('alumno')->group(function () {
        Route::get('all', [AlumnoController::class, 'all']);
        Route::post('edit', [AlumnoController::class, 'edit']);
        Route::post('add', [AlumnoController::class, 'add']);
        Route::get('apoderado/{id}', [AlumnoController::class, 'getApoderado']);
        Route::get('cursos/{id}', [AlumnoController::class, 'cursos']);
        Route::get('participantes/{id}', [AlumnoController::class, 'participantes']);
        Route::get('nuevos', [AlumnoController::class, 'nuevos']);
        Route::get('delete/{id}', [AlumnoController::class, 'delete']);
        Route::get('midia/{id}', [AlumnoController::class, 'miDia']);
        Route::get('horario/{id}', [AlumnoController::class, 'horario']);
        Route::get('news', [AlumnoController::class, 'news']);
        Route::get('validateImport', [AlumnoController::class, 'validateImport']);
    });
    
    Route::prefix('profesor')->group(function () {
        Route::get('all', [ProfesorController::class, 'all']);
        Route::get('activos', [ProfesorController::class, 'activos']);
        Route::post('add', [ProfesorController::class, 'add']);
        Route::post('edit', [ProfesorController::class, 'edit']);
        Route::get('delete/{id}', [ProfesorController::class, 'delete']);
        Route::get('aulas/{id}', [ProfesorController::class, 'aulas']);
        Route::get('midia/{id}', [ProfesorController::class, 'miDia']);
        Route::get('horario/{id}', [ProfesorController::class, 'horario']);
        Route::get('grados', [ProfesorController::class, 'grados']);
    });
    
    Route::prefix('admin')->group(function () {
        Route::get('all', [AdminController::class, 'all']);
        Route::post('add', [AdminController::class, 'add']);
        Route::post('edit', [AdminController::class, 'edit']);
        Route::get('delete/{id}', [AdminController::class, 'delete']);
    });
    
    
    
    Route::prefix('area')->group(function () {
        Route::get('all', [AreaController::class, 'all']);
        Route::post('add', [AreaController::class, 'add']);
        Route::post('edit', [AreaController::class, 'edit']);
    });
    
    Route::prefix('curso')->group(function () {
        Route::get('all', [CursoController::class, 'all']);
        Route::get('activos', [CursoController::class, 'activos']);
        Route::post('add', [CursoController::class, 'add']);
        Route::post('eliminar', [CursoController::class, 'eliminar']);
        Route::post('editar', [CursoController::class, 'editar']);
        Route::get('aulas/{id}', [CursoController::class, 'aulas']);
    });
    
    Route::prefix('capacidad')->group(function () {
        Route::get('all', [CapacidadController::class, 'all']);
        //Route::post('add', [CursoController::class, 'add']);
    });
    
    Route::prefix('competencia')->group(function () {
        Route::get('all', [CompetenciaController::class, 'all']);
        //Route::post('add', [CursoController::class, 'add']);
    });
    
    Route::prefix('aula')->group(function () {
        Route::post('', [AulaController::class, 'store']);
        Route::get('all', [AulaController::class, 'all']);
        Route::get('{id}', [AulaController::class, 'find']);
        Route::get('{id}/delete', [AulaController::class, 'delete']);
        Route::post('curso', [AulaCursoController::class, 'store']);
        Route::post('curso/profesor', [AulaCursoProfesorController::class, 'store']);
        Route::post('curso/dia', [DiaCursoController::class, 'store']);
        Route::post('alumno', [AlumnoAulaController::class, 'store']);
        Route::post('alumnos', [AlumnoAulaController::class, 'addBatch']);
        Route::post('cursos', [AulaController::class, 'storeCursos']);
        Route::post('validacion', [AulaController::class, 'validateSchedule']);
    });
    
    Route::prefix('curso')->group(function () {
        Route::post('profesor', [AulaCursoProfesorController::class, 'store']);
        Route::post('dia', [DiaCursoController::class, 'store']);
        Route::get('getContenido', [ContenidoController::class, 'getcontenido']);
        Route::post('saveContenido', [ContenidoController::class, 'addcontenido']);
        Route::post('deleteContenido', [ContenidoController::class, 'deleteContenido']);
        Route::post('actualizarCarpeta', [ContenidoController::class, 'actualizarCarpeta']);
        Route::post('addCarpeta', [ContenidoController::class, 'addCarpeta']);
        Route::get('getcontenidoBySubperiodo', [ContenidoController::class, 'getcontenidoBySubperiodo']);
    });
    
    Route::prefix('grado')->group(function () {
        Route::get('all', [GradoController::class, 'all']);
    });
    
    Route::prefix('nivel')->group(function () {
        Route::get('all', [NivelController::class, 'all']);
    });
    
    Route::prefix('dia')->group(function () {
        Route::get('all', [DiaController::class, 'all']);
    });
    
    Route::prefix('upload')->group(function () {
        Route::post('image', [UploadController::class, 'image']);
        Route::post('video', [UploadController::class, 'video']);
        Route::post('file', [UploadController::class, 'file']);
    });
    
    Route::prefix('configuracion')->group(function () {        
        Route::post('add', [ConfiguracionController::class, 'add']);
        Route::post('edit', [ConfiguracionController::class, 'edit']);
    });
});
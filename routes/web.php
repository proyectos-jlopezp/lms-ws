<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ConfiguracionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/usuarioCreado', function () {
    return view('usuarioCreado');
});

Route::prefix('configuracion')->group(function () {
    Route::get('all', [ConfiguracionController::class, 'all']);
});

Route::middleware('guest')->group(function () {
    Route::post('login', [AuthController::class, 'login']);
});


